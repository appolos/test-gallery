const express = require('express');
const logger = require('morgan');
const passport = require('./server/middleware/auth')
const sequelize = require('./server/models')
require('dotenv').config()
// Set up the express app
const app = express();

// Log requests to the console.
app.use(logger('dev'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(passport.initialize());

/* Protected Route */
app.get('/protected', passport.authenticate('jwt', { session: false }), function (req, res) {
    res.json('Success! You can now see this without a token.');
});
// Setup a default catch-all route that sends back a welcome message in JSON format.
app.get('*', (req, res) => res.status(200).send({
    message: 'Welcome to the beginning of nothingness.',
}));


module.exports = app;