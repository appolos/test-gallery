'use strict';
module.exports = (sequelize, DataTypes) => {
  const ImageTag = sequelize.define('ImageTag', {
    image_id: DataTypes.STRING,
    tag_id: DataTypes.STRING
  }, {});
  ImageTag.associate = function(models) {
    // associations can be defined here
  };
  return ImageTag;
};