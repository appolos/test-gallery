'use strict';
module.exports = (sequelize, DataTypes) => {
  const Image = sequelize.define('Image', {
    title: DataTypes.STRING,
    src: DataTypes.STRING,
    download_url: DataTypes.STRING,
    slug: DataTypes.STRING
  }, {});
  Image.associate = function(models) {
    Image.hasMany(models.Tag, {
      foreignKey: 'tag',
      as: 'tags',
    });
  };
  Image.hook('beforeSave', (user) => {
    user.slug = _.trim(user.name);}
    )
  return Image;
};