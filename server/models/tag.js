'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define('Tag', {
    title: DataTypes.STRING,
    slug: DataTypes.STRING
  }, {});
  Tag.associate = function (models) {
    Tag.belongsToMany(models.Image, { 
      through: models.ImageTag,
      as:'tags',
      foreignKey: 'tag_id',
    })
  };
  return Tag;
};